## 鱼 https://discuss.flarum.org.cn/d/2894/34

```html
<div id="flyfish" class="fly-fish" height="200"></div>
<script src="https://discuss.flarum.org.cn/fish.js"></script>
```

```css
.site-footer {
	margin:0;
	/*margin-top:64px;*/
	padding-top:30px;
	padding-bottom:30px;
	color:hsla(0,0%,100%,.9);
	box-sizing:inherit;
	text-align:center;
	font-size:1em
}
@media (max-width:991px){
    .site-footer {
	padding-top:0px;
    }
}
.site-footer p {
	margin:0
}
.rm-link-color > p > a {
	text-decoration:none;
	color:#89898c
}
.rm-link-color > p > a:hover {
	text-decoration:underline
}

.fly-fish{
    z-index: -1;
    line-height: 0px;
    height: 350px;
}
footer{
    position: relative;
}
.site-footer{
    position: absolute;
    width: 100%;
    top: calc(100% / 4 * 3);
    transform: translateY(calc(-50% + 10%));
    padding: 0;
}

.App{
    padding-bottom: 0;
}
@media (max-width: 767px){
    .App{
        padding-bottom: 0;
    }
    .App-content {
        padding-bottom: 0;
    }
}
```

## 桌面端顶栏高斯模糊

https://discuss.flarum.org.cn/d/4120
内嵌网易云音乐 https://discuss.flarum.org.cn/d/47
头像装饰 https://discuss.flarum.org/d/32500-decoration-store/2
浏览历史 https://discuss.flarum.org/d/32062-view-history
卡片帖子 https://packagist.org/packages/dem13n/discussion-cards
## plugin

- docker exec -ti flarum extension require flarum-lang/chinese-simplified
- docker exec -ti flarum extension require fof/linguist https://discuss.flarum.org/d/7026-linguist-customize-translations-with-ease/154
- docker exec -ti flarum extension require justoverclock/flarum-ext-contactme https://github.com/justoverclockl/flarum-ext-contactme
-  code diff https://packagist.org/packages/the-turk/flarum-diff
- composer require zerosonesfun/composer-preview  回复实时预览 https://packagist.org/packages/zerosonesfun/composer-preview
- Fancybox 超好用的图片灯箱 composer require darkle/fancybox
- FoF Follow Tags 关注标签 composer require fof/follow-tags
- FoF Links 导航栏链接 composer require fof/links



## more
https://extiverse.com/
https://discuss.flarum.org/t/extensions